latex_pub_list

If you want to create a simple list of publications, this is
how I did it. This might work for you if you have a bibliography
in a .bib file and BiBTeX is your workflow.

Read the PDF (which includes a publication list) for what it looks
like, and read the .tex to see how. Read the .bst for the gory
details.

